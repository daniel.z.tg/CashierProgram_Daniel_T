package me.danielzgtg.compsci11_sem2_2017.cashierprogram;

import java.util.Scanner;

/**
 * A simple program for cashiers
 * 
 * @author Daniel Tang
 * @since 6 Feb 2017
 */
public final class CashierProgram {

	/**
	 * Prompt layout before user input
	 */
	private static final String PRICE_PROMPT =
			"Please enter the price for item %d: ";

	/**
	 * Output layout for number to be substituted in
	 */
	private static final String OUTPUT_FORMAT = "\n"
			+ "Item 1: $%.2f\n"
			+ "Item 2: $%.2f\n"
			+ "Item 3: $%.2f\n"
			+ "Item 4: $%.2f\n"
			+ "Item 5: $%.2f\n"
			+ "\n"
			+ "Subtotal: $%.2f\n"
			+ "Tax: $%.2f\n"
			+ "Total: $%.2f\n";

	/**
	 * Tax rate to be applied to subtotal
	 */
	private static final double TAX_RATE = 0.13;

	public static void main(final String[] ignore) {
		final double price1, price2, price3, price4, price5;
		try (Scanner scanner = new Scanner(System.in)) {
			// Obtain user input from console, one price at a time
			System.out.format(PRICE_PROMPT, 1);
			price1 = scanner.nextDouble();

			System.out.format(PRICE_PROMPT, 2);
			price2 = scanner.nextDouble();

			System.out.format(PRICE_PROMPT, 3);
			price3 = scanner.nextDouble();

			System.out.format(PRICE_PROMPT, 4);
			price4 = scanner.nextDouble();

			System.out.format(PRICE_PROMPT, 5);
			price5 = scanner.nextDouble();
		}

		// Calculate subtotal, tax and total
		final double subtotal, tax, total;
		subtotal = price1 + price2 + price3 + price4 + price5;
		tax = subtotal * TAX_RATE;
		total = subtotal + tax;

		// Display everything
		System.out.format(OUTPUT_FORMAT,
				price1, price2, price3, price4, price5,
				subtotal, tax, total);
	}
}
